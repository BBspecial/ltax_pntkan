import React, { Component } from 'react';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Home from './Components/Home'
import './Style.css'

class App extends Component {
  render() {
    return (
      <Router>
        <Route
          render={({ location }) => (
            <Switch location={location}>
              <Route exact path="/" component={Home} />
            </Switch>
          )}
        />
      </Router>
    );
  }
}

export default App;
