import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Tile as TileLayer } from "ol/layer";
import { OSM } from "ol/source";
import Map from "../Components/Map";
import XYZSource from "ol/source/XYZ";

// import { Link } from 'react-router-dom'

class Home extends React.Component {
  render() {
    const baseLayer = new TileLayer({
      source: new OSM()
    });
    // const baseLayerGoogleSat = new TileLayer({
    //   preload: 4, //Infinity,
    //   source: new XYZSource({
    //     url: `http://mt0.google.com/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}`,
    //     attributions: "© google map"
    //   })
    // });
    const baseLayerGoogleStreet = new TileLayer({
      preload: 4, //Infinity,
      source: new XYZSource({
        url: `https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}`,
        attributions: "© google map"
      })
    });
    // XYZ Layer https://www.spatialbias.com/2018/02/qgis-3.0-xyz-tile-layers/
    return <div>Home</div>
    // return <Map baseLayer={baseLayerGoogleStreet} divStyle={{ height: "100vh" }} />;
  }
}

const mapStateToProps = ({}) => {
  // const { xxx } = xxxReducer
  return {};
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      //actions
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
