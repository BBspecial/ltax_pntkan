import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
// import { pick } from 'lodash'
import Olmap from 'ol/Map.js'
import View from 'ol/View.js'
import {bindActionCreators} from 'redux'
import { setBaseLayer, setInitial } from '../Actions/Actionmap'
import {ScaleLine} from 'ol/control.js';

class Map extends Component {
  static propTypes = {
    divMap: PropTypes.string.isRequired,
    mapClassName: PropTypes.string,
    divStyle: PropTypes.object,
    defaultZoomControls: PropTypes.bool,
    baseLayer: PropTypes.object.isRequired
  }

  static defaultProps = {
    divMap: 'map',
    mapClassName: '',
    divStyle: {},
    defaultZoomControls: true
  }

  componentDidMount() {
    this.configureMap()
  }

  async configureMap() {
    const map = new Olmap({
      target: this.props.divMap,
      view: new View({
        projection: 'EPSG:4326',
        center: [100.536315, 13.725831],
        zoom: 6
      })
    })
    // await this.visibleDefaultControls(map)
    await map.addControl(new ScaleLine())
    await this.props.setInitial(map)
    await this.props.setBaseLayer(this.props.baseLayer)
  }

  visibleDefaultControls(map) {
    if (!this.props.defaultZoomControls) {
      let listControl = map.getControls().array_
      let zoomControl = listControl.filter(
        control => control.constructor.name === 'Zoom'
      )[0]
      map.removeControl(zoomControl)
    }
    return map
  }

  render() {
    const { divMap, divClassName, divStyle } = this.props
    return (
      <div id={divMap} style={divStyle} className={divClassName}>
        <div style={{ position: 'absolute', zIndex: 1000 }}>
          {this.props.children}
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({olmap}) =>{
    const { divMap } = olmap
    return { divMap }
}

const mapDispatchToProps = dispatch => bindActionCreators ({
    setInitial, setBaseLayer
},dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Map)