import { combineReducers } from 'redux'
import { olmap } from './Olmap'

export default combineReducers({
    olmap
})