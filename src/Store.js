import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import reducers from './Reducers'

const middleware = [thunk]

if (process.env.NODE_ENV !== 'production') {
    middleware.push(createLogger())
}

// const Store = createStore(reducers, applyMiddleware(...middleware))

// export default Store






const configureStore = () => {
    const store = createStore(reducers, applyMiddleware(...middleware))
  
    // store.subscribe(() => {
    //   saveState(store.getState())
    // })
  
    if (module.hot) {
      module.hot.accept('./Reducers', () => {
        const nextRootReducer = require('./Reducers').default
        store.replaceReducer(nextRootReducer)
      })
    }
  
    return store
  }
  
  export default configureStore()